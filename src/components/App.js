import React from 'react';
import './App.css';
import Home from './Home/Home';

import Header from '../shared/components/layout/Header';
import Content from '../shared/components/layout/Content';
import Footer from '../shared/components/layout/Footer';

function App() {
	return (
		<div className="App">
			<Header title='Welcome to Rusky.com'/>
			<Content>
				<Home />
			</Content>
			<Footer />
		</div>
	);
}

export default App;
