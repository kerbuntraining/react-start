import React, { Component } from 'react';
import './Home.css';

class Home extends Component {
    constructor(){
        super();
        this.state = {
            name:'Facundo',
        };
    }
    componentDidMount(){
        setTimeout(()=>{
            this.setState({
                name :'Alvaro'
            });
        },1000);
    }
	render() {
        //style object
        // const buttonStyle = {
        //     backgroundColor: 'gray',
        //     border: '1px solid black'
        // };
        
		return (
			<div className='Home'>
				{/* <h1>Home!!!</h1>
				<p>
					You can visit our web site at
					<a href="http://www.kerbun.com/">Kerbun</a>.
				</p>
                <p>
                    <button style={buttonStyle}>Click me!!</button>
                </p> */}
                <p>My name is {this.state.name}</p>
			</div>
		);
	}
}

export default Home;
