import React from 'react';

const Footer = () => <footer>&copy; Rusky {new Date().getFullYear()}</footer>;

// class Footer extends Component {
//     render() {
//         return(
//             <footer>&copy; Rusky {(new Date()).getFullYear()}</footer>
//         );

//     }
// }

export default Footer;
